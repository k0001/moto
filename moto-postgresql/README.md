moto-postgresql
===============

This library provides a PostgreSQL-based migrations registry for the
`moto` migrations library.
