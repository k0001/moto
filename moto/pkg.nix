{ mkDerivation, aeson, attoparsec, base, bytestring, containers
, cryptohash-sha1, df1, di, di-core, di-df1, directory, filepath
, mtl, optparse-applicative, pipes, pipes-aeson, pipes-attoparsec
, pipes-bytestring, random, safe-exceptions, stdenv, tasty
, tasty-hunit, tasty-quickcheck, text, time, transformers
}:
mkDerivation {
  pname = "moto";
  version = "0.0.4";
  src = ./.;
  libraryHaskellDepends = [
    aeson attoparsec base bytestring containers cryptohash-sha1 df1
    di-core di-df1 directory filepath mtl optparse-applicative pipes
    pipes-aeson pipes-attoparsec pipes-bytestring safe-exceptions text
    time transformers
  ];
  testHaskellDepends = [
    base bytestring containers di di-core directory filepath random
    safe-exceptions tasty tasty-hunit tasty-quickcheck text time
  ];
  homepage = "https://gitlab.com/k0001/moto";
  description = "General purpose migrations library";
  license = stdenv.lib.licenses.asl20;
}
