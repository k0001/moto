{ pkgs }:

# To be used as `packageSetConfig` for a Haskell pacakge set:
let
src_di =
  builtins.fetchGit {
    url = "https://github.com/k0001/di";
    rev = "adf1aa972546477b8848e8199a2a976646885a1f";
  };

inherit (pkgs.haskell.lib) dontCheck doJailbreak;

in
pkgs.lib.composeExtensions
  (import "${src_di}/hs-overlay.nix" { inherit pkgs; })
  (self: super: {
    hspec-core = doJailbreak super.hspec-core;
    time-compat = doJailbreak super.time-compat;
    unix-time = doJailbreak super.unix-time;
    QuickCheck = super.callHackage "QuickCheck" "2.13.2" {};

    moto = super.callPackage ./moto/pkg.nix {};
    moto-example = super.callPackage ./moto-example/pkg.nix {};
    moto-postgresql = super.callPackage ./moto-postgresql/pkg.nix {};

    _shell = self.shellFor {
      withHoogle = true;
      nativeBuildInputs = [ self.cabal-install ];
      packages = p: [
        p.moto
        p.moto-example
        p.moto-postgresql
      ];
    };
  })
