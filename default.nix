# This file exports every derivation introduced by this repository.
{ nixpkgs ? import ./nixpkgs.nix }:
let pkgs = import ./pkgs.nix { inherit nixpkgs; };
in
pkgs.releaseTools.aggregate {
  name = "everything";
  constituents = [
    pkgs._here.ghc865.moto
    pkgs._here.ghc865.moto.doc
    pkgs._here.ghc865.moto-example
    pkgs._here.ghc865.moto-postgresql
    pkgs._here.ghc865.moto-postgresql.doc
    pkgs._here.ghc865._shell
  ];
}

